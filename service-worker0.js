var iMsgSendCounter=0;
console.log("Service Worker 0");

// Register event listener for the 'push' event.
self.addEventListener('push', function(event) {
  // Keep the service worker alive until the notification is created.
  const payload = event.data ? event.data.text() : "No message Body";
  console.log(payload);
/*  event.waitUntil(
    // Show a notification with title 'ServiceWorker Cookbook' and body 'Alea iacta est'.
    self.registration.showNotification('Agents Container Gateway', {
      body: payload ,
    })
  );*/
	send_message_to_all_clients({
		msgType:"SW-PUSHRECEIVED",
		msgContent:payload,
	});
		
});

function send_message_to_client(client, msg){
    return new Promise(function(resolve, reject){
        var msg_chan = new MessageChannel();

        msg_chan.port1.onmessage = function(event){
            if(event.data.error){
                reject(event.data.error);
            }else{
                resolve(event.data);
            }
        };

        client.postMessage(msg, [msg_chan.port2]);
    });
}
function send_message_to_all_clients(msg){
	msg.msgId=iMsgSendCounter;
	iMsgSendCounter++;
    clients.matchAll({
    includeUncontrolled: true
	}).then(clients => {
        clients.forEach(client => {
            send_message_to_client(client, msg);//.then(m => console.log("SW Received Message: "+m));
        })
    })

}
self.addEventListener('message', function(event){
    console.log("SW 0 Received Message: " + event.data);
    event.ports[0].postMessage("SW 0 Says 'Hello back!'");
});
self.addEventListener('activate', function(event) {
	console.log("Activated SW 0");
	event.waitUntil(self.clients.claim());
	send_message_to_all_clients({
		msgType:"SW-ALIVE",
		msgContent:"Hello I'm the service worker 0... i´m alive!",
	});
});
